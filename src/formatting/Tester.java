package formatting;

public class Tester {
    public static void main(String[] args) {
        int x = 8;
        int y = 3;
        System.out.printf("%d + %d \n", y, x); // stmt1
        System.out.printf("%f + %f \n", (float) y, (float) x);// stmt2
        System.out.printf("%d + %d \n", x, y);// stmt3
        System.out.format("%2$d + %1$d", x, y);// stmt4
    }
}
