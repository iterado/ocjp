package datatypes;

public class Tester4 {
    public static void main(String agr[]) {
        short s1 = 4; // LINE 1
        short s2 = s1 += s1; // LINE 2
        short s3 = s1 + s2; // LINE 3
        byte b1 = (byte) s1 + (byte) s2; // LINE 4
        byte b2 = (byte) ((byte) s1 + (byte) (byte) s2); // LINE 5
    }
}