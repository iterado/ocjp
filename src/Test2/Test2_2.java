package Test2;

import java.io.File;

public class Test2_2 {

    public static void main(String[] args) {

        String name = "testDir" + File.separator + "subDir2" + File.separator + "Shackelton.txt";
        File f = new File(name);
        System.out.println("exists " + f.exists());
    }
}
// 2_2
// A, Line 12 is correct as it stands (String name...)
// C, The program must be invoked from the baseDir directory.
// E, Line 12 must use File.separator instead of File.pathSeparator