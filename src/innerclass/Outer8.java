package innerclass;

class Outer8 {
    private int i = 5; // line 1

    Outer8(int i) { // line 2
        this.i = i; // line 3
        System.out.print(++i); // line 4
    }

    class Inner {
        Inner() {
            System.out.print("," + i++); // line 5
        }
    }

    public static void main(String[] args) {
        int i = 6;
        Outer8.Inner in = new Outer8(i).new Inner(); // line 6
    }
}
