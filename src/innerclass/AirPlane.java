package innerclass;

public class AirPlane {
    public void fly(int speed) {
        final int e = 1;
        class FlyingEquation {
            {
                System.out.println(e);// line 1
                System.out.println(speed);// line 2
            }
        }
    }
}