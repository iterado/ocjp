package innerclass;

public class OuterTest2 {
    public static void main(String args[]) {
        Airplane2.BlackBox box = new Airplane2().new BlackBox(); // line 1
        box.printVariables();
    }
}

class Airplane2 {
    String code = "11";

    class BlackBox {
        String code = "22";

        public void printVariables() {
            System.out.print(code);
            System.out.print(Airplane2.this.code); // line 20
        }
    }
}