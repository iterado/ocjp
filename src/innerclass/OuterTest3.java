package innerclass;

//import innerclass.Airplane3.BlackBox;

public class OuterTest3 {
    public static void main(String args[]) {
        // instantiation 1
        Airplane3 airplane = new Airplane3();
        Airplane3.BlackBox box1 = airplane.new BlackBox();

        // instantiation 2
        Airplane3.BlackBox box2 = new Airplane3().new BlackBox();

        // instantiation 3
        Airplane3 airplane3 = new Airplane3();
        BlackBox box3 = airplane3.new BlackBox();
    }
}

class Airplane3 {
    class BlackBox {
    }
}