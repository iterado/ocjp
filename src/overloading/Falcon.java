package overloading;

class Creature {
    Creature getIt() {
        return this;
    }
}

class Bird extends Creature {
    // insere code here
    // Creature getIt() { return this;}
    // private Falcon getIt() { return new Falcon();}
    // Falcon getIt() {return this;}
    // public Bird getIt() {return this;}
    // Creature getIt(Creature c) {return this;}
}

class Falcon extends Bird {
}