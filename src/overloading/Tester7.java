package overloading;

class Base {
    public void method(final int x) {
        System.out.print("Base");
    }
}

public class Tester7 extends Base {
    public void method(int x) { // line 1
        System.out.print("Tester7");
    }

    public static void main(String[] args) {
        Base b = new Tester7();
        b.method(3);
    }
}
