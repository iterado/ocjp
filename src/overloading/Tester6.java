package overloading;

class Base {
    void call() throws IllegalArgumentException {
    }
}

public class Tester6 extends Base {
    // public void call() throws IllegalArgumentException {}
    // void call() throws IllegalArgumentException,FileNotFoundException {}
    // void call() throws RuntimeException {}
    // void call() throws IllegalArgumentException,RuntimeException { }
    // private void call() {}
}