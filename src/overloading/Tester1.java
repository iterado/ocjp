package overloading;

class Type {
    public Object getTypeName() {
        return "Type";
    }
}

class SubType extends Type {
    public String getTypeName() {// line 8
        return "SubType";
    }
}

public class Tester1 {
    public static void main(String[] args) {
        Type first = new SubType(); // line 16
        System.out.println(first.getTypeName()); // line 17
    }
}
