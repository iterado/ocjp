package overloading;


class Test {
    Test() {
        System.out.println("Hello");
    }

    public void showItem() {

    }
}

abstract class A extends Test {
    A() {
        System.out.println("Hi everyOne");
    }

    abstract public void showItem();
}

class Dont extends A {
    public void showItem() {
    }

    public static void main(String... args) {
        new Dont();
    }
}
