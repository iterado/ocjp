package overloading;

class MySuper {
    MySuper() {
        disp();
    }

    void disp() {
        System.out.println("superclass");
    }
}

class MySub extends MySuper {
    double i = Math.ceil(8.4f);

    public static void main(String arg[]) {
        MySuper obj = new MySub();
        obj.disp();
    }

    void disp() {
        System.out.println(i);
    }
}