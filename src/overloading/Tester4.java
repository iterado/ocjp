package overloading;

public class Tester4 {

    static void call(Long x, Long y) {
        System.out.print("Long x, Long y");
    }

    static void call(int... x) {
        System.out.print("int... x");
    }

    public static void main(String[] args) {
        int val = 3;
        call(val, val);
    }
}
