package exceptions;

class Father2 {
    public Father2() throws RuntimeException {
        System.out.print("Father");
        throw new RuntimeException();
    }
}

class Son2 extends Father {
    public Son2() throws RuntimeException {
        System.out.print("Son");
    }
}

public class Tester2 {
    public static void main(String[] args) {
        new Son2(); // line 17

    }
}