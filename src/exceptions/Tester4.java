package exceptions;

import java.io.IOException;

class AirPlane4 {
    public AirPlane4() throws IOException, RuntimeException {
        System.out.println("AirPlane");
    }
}

class AirJet4 extends AirPlane4 {
} // line 7

public class Tester4 {
    public static void main(String args[]) throws IOException { // line 10
        new AirPlane4(); // line 11
    }
}
