package exceptions;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class Tester11 {
    static void call() throws IOException { // line 3
        File file = new File("javachamp.dat"); // line 4
        file.createNewFile(); // line 5
        throw new IllegalArgumentException(); // line 6
    }

    public static void main(String[] args) throws ParseException {
        try {
            call();
            DateFormat df = DateFormat.getDateInstance(); // line 12
            Date parse = df.parse("12.11.2009"); // line 13
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } // line 11
    }
}
