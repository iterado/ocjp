package exceptions;

import java.io.IOException;

class Father {
    public Father() throws IOException {
        System.out.print("Father");
        throw new IOException();
    }
}

class Son extends Father2 {
    public Son() throws IOException {
        System.out.print("Son");
    }
}

public class Tester1 {
    public static void main(String[] args) {
        try {
            new Son2();
        } catch (IOException e) {
            System.out.print("Inside catch");
        }
    }
}
