package Test1;

public class Jail {
    private int x = 4;

    public static void main(String[] args) {
        int x = 6;
        new Jail().new Cell().slam();
    }

    class Cell {
        void slam() {
            System.out.println("throw away key " + x);
        }
    }
}
// 1.2
// D, because of local variable can't be protected
// Now the correct answer is C