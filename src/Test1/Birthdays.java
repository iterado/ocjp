package Test1;

import java.util.HashMap;
import java.util.Map;

public class Birthdays {
    public static void main(String[] args) {
        Map<Friends, String> hm = new HashMap<Friends, String>();
        hm.put(new Friends("Charis"), "Summer 2009");
        hm.put(new Friends("Draumur"), "Spring 2002");
        Friends f = new Friends("Draumur");
        System.out.println(hm.get(f));
    }
}

class Friends {
    public String name;

    Friends(String n) {
        name = n;
    }
}
// 1.8
// A, because Friends class doesn't override equals and hashCode methods, so
// when we are creating object Friends which is a key of the map, and next we
// create another object Friends, it is a new object, and they are two different
// keys, because we can't compare it correctly.