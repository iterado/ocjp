package Test1;

class Feline {
}

public class BarnCat2 extends Feline {
    public static void main(String[] args) {
        Feline ff = new Feline();
        BarnCat2 b = new BarnCat2();
        // insert code here
        if (b instanceof Feline)
            System.out.print("3 ");
    }
}
// 1.4
// C, because of only C has correct syntax and b can be instance of class, not
// instance of object, and b is instance of Feline, because of inheritance