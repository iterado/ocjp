package Test1;

public class Test1_13 {
    public static void main(String[] args) {
        boolean b = false;
        int i = 7;
        double d = 1.23;
        float f = 4.56f;

        System.out.printf(" %b", b);
        // System.out.printf(" %i", i); //it should be "%d"
        // System.out.format(" %d", d); //it should be %f
        System.out.format(" %d", i);
        System.out.format(" %f", f);
    }
}
// A, D, E