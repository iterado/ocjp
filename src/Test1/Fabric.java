package Test1;

public class Fabric extends Thread {
    public static void main(String[] args) {
        Thread t = new Thread(new Fabric());
        Thread t2 = new Thread(new Fabric());
        t.start();
        t2.start();
    }

    public void run() {
        for (int i = 0; i < 2; i++)
            System.out.print(Thread.currentThread().getName() + " ");
    }
}
// 1.3
// A, because of run is inherit method from Thread and it can't be static
// Now the correct answers could be D, E, F