package threads;

class SyncDemo {
    public static void main(String... args) {
        class A implements Runnable {
            public synchronized void run() {
                display();
            }

            synchronized void display() {
                for (int i = 0; i < 5; i++) {
                    System.out.print("Hello");
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                    }
                    System.out.println(Thread.currentThread().getName());
                }
            }
        }

        A ob1 = new A();
        A ob2 = new A();
        Thread ob3 = new Thread(ob1, "Saurabh");
        Thread ob4 = new Thread(ob2, "Nikhil");
        ob3.start();
        ob4.start();
    }
}