package threads;

public class Test8 extends Thread {
    static int count = 0;

    public static void main(String argv[]) throws InterruptedException {
        Test8 t = new Test8();
        t.increment(count);
        t.start();
        Thread.sleep(1000);
        System.out.println(count);
    }

    public void increment(int count) {
        ++count;
    }

    public void run() {
        count = count + 5;
    }
}
