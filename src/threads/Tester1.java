package threads;

class Tester1 extends Thread {
    int total;

    public static void main(String[] args) throws Exception {
        Tester1 t = new Tester1();
        t.start();
        System.out.println("hi how are you:");
        synchronized (t) {
            System.out.println("waiting for t to complete");
            t.wait();
            System.out.println("total" + t.total);
        }
    }

    synchronized public void run() {
        for (int i = 0; i < 3; i++) {
            total = total + i;
        }
    }
}
