package threads;

public class Test21 {
    public static void main(String[] args) {
        new Thread(); // line 1
        new Thread("myThread"); // line 2
        new Thread(new Long(14)); // line 3
        new Thread(new Runnable() {
            public void run() {
            }
        }); // line 4
        Thread.getInstance(); // line 5
    }
}
