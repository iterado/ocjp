package threads;

public class Tester16 {
    public void validate() {
        int i = 0;
        while (++i < 3) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print(i);
        }
    }

    public static void main(String[] args) {
        new Tester16().validate();
    }
}
