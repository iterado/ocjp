package threads;

public class Test4 implements Runnable {
    public static void main(String[] args) throws InterruptedException {
        Test4 test = new Test4();
        Thread t = new Thread(test);
        t.start();
        t.join();
        System.out.print("main");
    }

    public void run() {
        System.out.print("run");
    }
}