package threads;

public class Test12 implements Runnable {
    int id;
    Test12(int id) {
        this.id = id;
    }
    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(new Test12(1));
        Thread thread2 = new Thread(new Test12(2));
        thread1.run();
        thread2.start();
        System.out.print("main");
    }
    public void run() {
        System.out.print(id);
    }
}
