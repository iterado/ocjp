package threads;

public class Test6 implements Runnable {
    Integer id = 0;

    public static void main(String[] args) {
        new Thread(new Test6()).start();
        new Thread(new Test6()).start();
    }

    public void run() {
        press(id);
    }

    synchronized void press(Integer id) {
        System.out.print(id.intValue());
        System.out.print((++id).intValue());
    }
}
