package threads;

public class Test3 {
    public static void main(String[] args) {
           Thread request1 = new Thread(new InternetRequest (),"request#1 ");
           Thread request2 = new Thread(new InternetRequest (),"request#2 ");
           
           request1.start();
           request2.start();
       }
   }
   class InternetRequest implements Runnable {
       
       public void run() {
           System.out.print(Thread.currentThread().getName());
       }
   }