package threads;

public class Painter{
    
    public static void main(String[] args) {
        Painter painter1 = new Painter();
        painter1.start();
        
        Painter painter2 = new Painter();
        painter2.start();
    }
}
class Painter implements Runnable {
    public void run() {
        System.out.println("we are painting");
    }
}
