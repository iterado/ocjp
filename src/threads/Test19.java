package threads;

public class Test19 {
    static Runnable t1 = new Runnable() {
        public void run() {
            try {
                System.out.print("t1before");
                Thread.sleep(100);
                System.out.print("t1after");
            } catch (InterruptedException e) {
            }
        }
    };
    static Thread t2 = new Thread() {
        public void run() {
            try {
                System.out.print("t2before");
                synchronized (this) {
                    wait();
                }
                System.out.print("t2after");
            } catch (InterruptedException e) {
            }
        }
    };

    public static void main(String[] args) throws InterruptedException {
        new Thread(t1).start();
        t2.start();
    }
}
