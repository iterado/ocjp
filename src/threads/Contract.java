package threads;

class Worker extends Thread {
    Contract contract;

    Worker(Contract contract) {
        this.contract = contract;
    }

    public void run() {
        contract.work();
    }
}

public class Contract {
    StringBuilder wall = new StringBuilder("brick");
    boolean isCementLastAdded = false;

    public void putBrick() {
        if (isCementLastAdded && !isWallDone()) {
            wall.append("brick");
            isCementLastAdded = false;
        }
        System.out.println(wall);
    }

    public void putCementLayer() {
        if (!isCementLastAdded && !isWallDone()) {
            wall.append("cement");
            isCementLastAdded = true;
        }
        System.out.println(wall);
    }

    public synchronized boolean isWallDone() {
        return wall.length() >= 100;
    }

    public synchronized void work() {
        while (!isWallDone()) {
            putCementLayer();
            putBrick();
        }
    }

    public static void main(String[] args) {
        Contract contract = new Contract();
        new Worker(contract).start();
        new Worker(contract).start();
    }
}
