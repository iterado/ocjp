package threads;

public class Test22 extends Thread {
    int id;
    Test22(int id) {
        this.id = id;
        start();
    }
    public static void main(String[] args)  {
        Thread t = new Thread(new Test22(2));
        t.start();
        System.out.print("main");
    }
    public void run() {
        System.out.print(id);
    }
}