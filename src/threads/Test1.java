package threads;

public class Test1 implements Runnable {
    Integer id;

    public static void main(String[] args) throws InterruptedException {
        new Thread(new Test1()).start();
        new Thread(new Test1()).start();
    }

    public void run() {
        press(id);
    }

    synchronized void press(Integer id) {
        System.out.print(id.intValue());
        System.out.print((++id).intValue());
    }
}