package interfaces;

public abstract interface Bouncable {

    int a = 0;
    public int b = 1;
    public static int c = 2;
    public static transient int d = 3;
    public final int e = 3;
    public static final int f = 3;
}