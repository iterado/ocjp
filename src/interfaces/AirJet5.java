package interfaces;

abstract class AirPlane { // line 1
    abstract void fly(); // line 2

    void land() {
        System.out.print("Landing..");
    }
}

class AirJet5 extends AirPlane { // line 10
    AirJet() {
        // super(); // line 13
    }

    void fly() {
        System.out.print("Flying..");
    }

    void land(); // line 20
}
