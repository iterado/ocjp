package interfaces;

public abstract class Tester8 {
    public void test1();

    public final void test2() {
    };

    public static void test3() {
    };

    public abstract static void test4();

    public abstract final void test5();
}