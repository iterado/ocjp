package inheritance;

interface Colorable {
}

class Vehicle {
}

class Car extends Vehicle6 implements Colorable6 {
}

public class Tester5 {
    public static void main(String[] args) {
        Vehicle6 a = new Car6();
        Colorable6 i = (Colorable6) a;
        Vehicle6 b = new Vehicle6();
        Colorable6 j = (Colorable6) b;
    }
}
