package inheritance;

interface Colorable6 {
}

class Vehicle6 {
}

class Car6 extends Vehicle6 implements Colorable6 {
}

public class Tester6 {
    public static void main(String[] args) {
        Vehicle6 a = new Car6();
        Colorable6 i = (Colorable6) a;
        Vehicle6 b = new Vehicle6();
        Colorable6 j = (Colorable6) b;
    }
}