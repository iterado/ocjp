package inheritance;

class Base {

    public Base(String n) {
        System.out.print("Base1");
    }

    // public Base() {
    // }

    public void Base(String n) {
        System.out.print("Base2");
    }

}

public class Tester12 extends Base {

    public Tester12() {
        System.out.print("Derived");
    }

    public static void main(String[] args) {
        new Tester12();
    }
}