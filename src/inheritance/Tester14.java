package inheritance;

class Creature15 {
    String getName() {
        return "Creature";
    }
}

class Bird15 extends Creature15 {
    String getName() {
        return "Bird";
    }
}

class Falcon15 extends Bird15 {
    String getName() {
        return "Falcon";
    }
}

public class Tester14 {
    public static Bird15 getIt(Creature15 c) {
        System.out.println(c.getName());
        return (Bird15) c;
    }

    public static void main(String[] args) {
        // insert code here
        // getIt(new Creature());
        getIt(new Bird15());
        getIt(new Falcon15());
        // getIt(new Object());

        System.out.println(new Falcon15() instanceof Creature15);
        System.out.println(new Falcon15() instanceof Bird15);

        System.out.println(new Creature15() instanceof Bird15); // Creature is not a
                                                            // Bird, so it
                                                            // cannot be cast to
                                                            // Bird

        // Creature tt = new Creature();
        // Bird dd = (Bird) tt;
        //
        // Falcon ff = new Falcon();
        // Bird bb = (Bird) ff;

    }
}