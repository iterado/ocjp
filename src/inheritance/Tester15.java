package inheritance;

class Creature15 {
}

class Bird15 extends Creature15 {
}

class Falcon15 extends Bird15 {
}

public class Tester15 {
    public static void main(String[] args) {
        Creature15 c1 = new Creature15();
        Creature15 c2 = new Bird15();
        Bird15 b1 = (Bird15) c1; // Line 1
        Bird15 b2 = (Falcon15) c2; // Line 2
        // Bird b3 = c2; // Line 3
        Bird15 b4 = new Falcon15(); // Line 4
        Bird15 b5 = (Bird15) new Creature15(); // Line 5
        // Falcon f1 = b4; // Line 6
    }
}
