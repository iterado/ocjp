package wrappers;

public class Tester4 {
    public static void main(String[] args) {
        Long var = (long) 999; // Line 3
        long x1 = var.longValue(); // Line 5
        double x2 = var.longValue(); // Line 7
        double x3 = (double) var.longValue(); // Line 9
        Double x4 = Long.valueOf("999"); // Line 11
        Number x5 = Integer.parseInt("999"); // Line 13
        Long x6 = Integer.parseInt("999"); // Line 15
    }
}
