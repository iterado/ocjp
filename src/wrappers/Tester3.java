package wrappers;

public class Tester3 {
    public static void main(String[] args) {
        Number x = 12; // Line 5
        Number y = (Long) x; // Line 6
        System.out.print(x + "" + y); // Line 7
    }
}
