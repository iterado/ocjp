package wrappers;

public class Tester10 {
    public static void main(String[] args) {
        int i1 = 120;
        int i2 = 180;
        Integer in1 = Integer.valueOf(i2);
        Integer in2 = Integer.valueOf(i2);
        System.out.print(in1 == in2);
        in1 = Integer.valueOf(i1);
        in2 = Integer.valueOf(i1);
        System.out.print(in1 == in2);
    }
}
