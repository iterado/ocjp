package wrappers;

public class Tester1 {

    public static void main(String[] args) {
        Integer sum1 = 125; // line 1
        int sum2 = 125; // line 2
        System.out.print(sum1.equals(sum2)); // line 3
        System.out.print(sum2.equals(sum1)); // line 4
        System.out.print(sum1 == sum2); // line 5
    }
}
