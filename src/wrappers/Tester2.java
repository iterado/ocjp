package wrappers;

public class Tester2 {

    static void calculate(Double i) {
    }

    static void calculate(Integer i) {
        System.out.println("Integer i");
    }

    static void calculate(int i) {
        System.out.println("int i");
    }

    static void calculate(int... i) {
        System.out.println("int... i");
    }

    static void calculate(Integer... i) {
        System.out.println("Integer... i");
    }

    public static void main(String[] args) {
        calculate(12);
    }
}
