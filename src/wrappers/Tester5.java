package wrappers;

public class Tester5 {
    public static void main(String[] args) {
        Integer[][] arr1 = { { 1, 2 }, { 3, 4 } };
        Number[] arr2 = arr1[0];
        int x = 1;
        System.out.print(arr1[0][0] == 1);
        System.out.print(arr2[0] == x);
        System.out.print(x instanceof Integer);
        System.out.print(arr1[1][0] > (Integer) arr2[0]);
    }
}
