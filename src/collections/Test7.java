package collections;

import java.util.Arrays;

public class Test7 {
    public static void main(String[] args) {

        String[] arr = { "java", "champ", "champion" };
        for (String s : arr) {
            System.out.println(s);
        }
        Arrays.sort(arr);
        System.out.println("==");
        for (String s : arr) {
            System.out.println(s);
        }
        System.out.print(Arrays.binarySearch(arr, "champion"));
        System.out.print(Arrays.binarySearch(arr, "You"));
    }
}
