package collections;

import java.util.NavigableMap;
import java.util.TreeMap;

public class Tester {

    public static void main(String[] args) {
        TreeMap tree = new TreeMap();
        tree.put("aa", 1);
        tree.put("cc", 2);
        tree.put("ee", 3);
        tree.put("gg", 4);

        System.out.println(tree);

        NavigableMap nvMap = tree.headMap("ee", false);

        System.out.println(nvMap);

        // w NavigableMap mozemy updatnac instniejacy klucz, nie mozemy dac
        // nowego
        nvMap.put("nn", 5); // line 16
        System.out.println(nvMap);
    }
}