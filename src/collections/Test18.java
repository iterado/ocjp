package collections;

import java.util.LinkedList;
import java.util.Queue;

public class Test18 {

    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(1);
        queue.add(3);
        queue.add(4);
        queue.add(7);

        System.out.println(queue);

        // insert code here
        // A
        // queue.poll();
        // System.out.println(queue);
        // queue.offer(0); // Inserts the specified element into this queue if
        // it
        // is possible to do so immediately without violating capacity
        // restrictions.
        // System.out.println(queue);
        // queue.peek();

        // B
        // queue.peek();
        // queue.add(0);
        // queue.peek(); //Retrieves, but does not remove, the head of this
        // queue, or returns null if this queue is empty.
        // queue.poll(); //Retrieves and removes the head of this queue, or
        // returns null if this queue is empty.

        // C
        // queue.add(0);
        // queue.remove(); //Retrieves and removes the head of this queue.
        // queue.peek();

        // D
        // queue.add(0);
        // queue.poll();
        // queue.remove();

        System.out.println(queue);
    }
}
