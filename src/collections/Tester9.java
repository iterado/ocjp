package collections;

import java.util.Arrays;

public class Tester9 {
    public static void main(String[] args) {
        Integer[] arr = { 1, 2, 3 };
        System.out.print(Arrays.binarySearch(arr, 1)); // line 1
        System.out.print(Arrays.binarySearch(arr, "javachamp")); // line 2
    }
}
