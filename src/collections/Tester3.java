package collections;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class Tester3 extends Thread {

    public static void main(String[] args) {
        Integer[] arr = { 7, 5, 7, 3 };
        System.out.println(Arrays.asList(arr));
        Set<Integer> set = new TreeSet<Integer>(Arrays.asList(arr));
        System.out.println(set);
        set.add(4);
        System.out.println(set);
        for (Integer value : set) {
            System.out.print(value);
        }
    }
}