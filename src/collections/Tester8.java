package collections;

import java.util.Arrays;

public class Tester8 {
    public static void main(String[] args) {
        String[] arr = { "java", "champ", "you" };
        System.out.print(Arrays.binarySearch(arr, "java"));
        System.out.print(Arrays.binarySearch(arr, "You"));
    }
}
