package collections;

import java.util.SortedSet;
import java.util.TreeSet;

public class Test17 {

    public static void main(String[] args) {
        TreeSet<Integer> map = new TreeSet<Integer>();
        map.add(1);
        map.add(2);
        map.add(4);
        map.add(7);
        SortedSet<Integer> smap = map.subSet(2, 7);
        map.add(5);
        map.add(9);
        System.out.println(smap);
    }
}
