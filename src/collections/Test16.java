package collections;

import java.util.SortedMap;
import java.util.TreeMap;

public class Test16 {

    public static void main(String[] args) {
        TreeMap<Integer, String> map = new TreeMap<Integer, String>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        System.out.println(map);
        SortedMap<Integer, String> smap1 = map.tailMap(2);
        System.out.println(smap1);
        SortedMap<Integer, String> smap2 = smap1.headMap(4);
        System.out.println(smap2);
        SortedMap<Integer, String> smap3 = smap2.subMap(2, 3); // Returns a view
                                                               // of the portion
                                                               // of this map
                                                               // whose keys
                                                               // range from
                                                               // fromKey,
                                                               // inclusive, to
                                                               // toKey,
                                                               // exclusive.
        System.out.println(smap3);
    }
}
