package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Test10 {
    public static void main(String[] args) {
        List<Human> humans = new ArrayList<Human>();
        humans.add(new Human(13));
        humans.add(new Human(33));
        humans.add(new Human(21));
        humans.add(new Human(21));
        Collections.sort(humans);
        System.out.println(humans);
        for (Human h : humans) {
            System.out.println(h.age);
        }
        System.out.print(humans.get(0).age);
        System.out.print(humans.size());
    }
}

class Human implements Comparable<Human> {
    Integer age;

    public Human(Integer age) {
        this.age = age;
    }

    public int compareTo(Human h) {
        System.out.println(h.age + "," + this.age + "," + h.age.compareTo(this.age));

        return h.age.compareTo(this.age);
        // return this.age.compareTo(h.age);
    }
}
