package collections;

import java.util.TreeMap;

public class Test14 {

    public static void main(String[] args) {
        TreeMap<Integer, String> map = new TreeMap<Integer, String>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        System.out.println(map);
        System.out.print(map.higherKey(2)); // Returns the least key strictly
                                            // greater than the given key, or
                                            // null if there is no such key.
        System.out.print(map.ceilingKey(2)); // Returns the least key greater
                                             // than or equal to the given key,
                                             // or null if there is no such key.
        System.out.print(map.floorKey(1)); // Returns the greatest key less than
                                           // or equal to the given key, or null
                                           // if there is no such key.
        System.out.print(map.lowerKey(1)); // Returns the greatest key strictly
                                           // less than the given key, or null
                                           // if there is no such key.
    }

}
