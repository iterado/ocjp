public class Tester {
    static int x = 4;

    public Tester() {
        final int d = 9;
        System.out.print(this.x); // line 4
        Tester(); // normal static method, not contructor
    }

    public static void Tester() { // line 8
        // System.out.print(this.x); // line 9
    }

    public static void main(String... args) { // line 12
        // new Tester();
        System.out.println(args[0]);

        int x1 = 1;
        int x2 = 2;
        int x3 = 3;
        int x4 = 4;
        System.out.println(x1 + "" + x2 + x3 + x4);
    }
}