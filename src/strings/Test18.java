package strings;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Test18 {
    public static void main(String[] args) {
        String jc = "javachamp_champ2012champ";
        Pattern p = Pattern.compile(".{4}c+(m)*"); // line 1
        Matcher m = p.matcher(jc);
        while (m.find()) {
            System.out.print(m.start());
        }
    }
}
