package strings;

import java.util.Scanner;

public class Test15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner("javachamp 2009, true 239");
        int i = 1;
        while (sc.hasNext()) {
            System.out.println("--" + i);
            if (sc.hasNextBoolean())
                System.out.print("Boolean");
            if (sc.hasNextInt())
                System.out.print("Int");
            sc.next();
            i++;
        }
    }
}
