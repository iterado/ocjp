package strings;

public class Test16 {
    public static void main(String[] args) {
        String str = "java";
        StringBuffer sb = new StringBuffer("javachamp");
        sb.insert(9, ".com");
        str.concat("champ");
        if (sb.length() < 6 || str.equals("javachamp")) {
            System.out.print(sb);
        }
        sb.delete(2, 7);
        System.out.print(sb);
    }
}
