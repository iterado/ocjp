package generics;

import java.util.ArrayList;
import java.util.List;

interface chewable3 {
}

class Gum3 implements chewable3 {
}

class Meat3 implements chewable3 {
}

public class Tester3 {
    public static void main(String[] args) {
        List list1 = new ArrayList<chewable>();// line 1
        list1.add(new Gum3());// line 2
        list1.add(new Meat3());// line 3
        list1.add(new Integer(9));// line 4
        System.out.println(list1.size());// line 5

        List<String> list = new ArrayList<String>();
        List<?> list2 = new ArrayList<String>();
        List<? extends Object> list3 = new ArrayList<String>();
        List<Object> list4 = new ArrayList<String>();
        List list5 = new ArrayList<String>();
        List list6 = new ArrayList<?>();
    }
}