package generics;

import java.util.ArrayList;
import java.util.List;

public class Tester9 {
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("Hello");
        Foo9 f = new Foo9();
        list.add(f);
        f = list.get(1);
        System.out.print(list.get(0) + "-" + f);
    }
}

class Foo9 {
    public String toString() {
        return "Foo";
    }
}
