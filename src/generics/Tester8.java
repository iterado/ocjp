package generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Tester8 {
    public static void main(String[] args) {
        List<Foo> myFooList = new ArrayList<Foo>();
        myFooList.add(new Foo("C"));
        myFooList.add(new Foo("A"));
        myFooList.add(new Foo("D"));
        Collections.sort(myFooList);
        System.out.print(myFooList.get(0).code);
    }
}

class Foo implements Comparable<Foo> {
    String code;

    Foo(String c) {
        code = c;
    }

    int compareTo(Foo f) {
        return this.code.compareTo(f.code);
    }
}