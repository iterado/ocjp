package enums;

enum Sounds
{
    HAM_HAM, CHIP_CHIP
}
interface PlaySounds
{
    void play();
}
enum AnimalSound extends Sounds implements PlaySounds 
{
    WOOF("dog"), meow("CaT"), 
    BURBLE("fish")
    {
        int nrOfLegs = 1;    // line 1
    };  
    public static int nrOfLegs = 4;
    private String animal;
    public getAnimal()  
    {
        return animal;
    }
    AnimalSound(String animal)
    {
         this.animal = animal;
    }
    
    void play(){}    
}    
public class Test10 
{
    public static void main(String[] args) 
    {
        for(AnimalSound sound: AnimalSound.values())
        {
            System.out.println("A " + sound.animal + " makes " + sound + " and has " + AnimalSound.nrOfLegs + " legs."); 
        }
    }
}