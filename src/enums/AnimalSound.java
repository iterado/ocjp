package enums;

static final enum AnimalSound {
    WOOF, meow, BURBLE // line 1
} // line 2

strictfp enum Animal {
    DOG(AnimalSound.woof, 4), // line 3
    CAT(MEOW, 4), // line 4
    FISH(AnimalSound.BURLE) // line 5
    {
        public int getNrOfLegs() {
            return 1;
        }
    }; // line 6

    private AnimalSound sound;
    private int nrOfLegs;

    Animal(AnimalSound sound, int nrOfLegs) {
        this(sound);
        this.nrOfLegs = nrOfLegs;
    }

    Animal(AnimalSound sound) {
        this.sound = sound;
    }

    public AnimalSound getSound() {
        return sound;
    }

    public int getNrOfLegs() {
        return nrOfLegs;
    }
}; // line 7
