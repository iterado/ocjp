package enums;

enum Status {
    Published(1), Rejected(2); // Line 2
    int index; // Line 3

    Status(int i) {
        index = i;
    }

    int getIndex() {
        return index;
    } // Line 5
}

public class Tester1 {
    public static void main(String[] args) {
        System.out.println(Status.Published.index + Status.Rejected.index); // Line
                                                                            // 10
    }
}
