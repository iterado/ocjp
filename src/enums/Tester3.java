package enums;

public class Tester3 {

    enum BookStatus {
        PUBLISHED, DRAFT
    }

    public static void main(String[] args) {
        BookStatus s1 = BookStatus.PUBLISHED;
        BookStatus s2 = BookStatus.PUBLISHED;
        System.out.print(s1 == BookStatus.PUBLISHED);
        System.out.print(s1.equals(s2));
    }
}
