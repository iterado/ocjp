package enums;

public class Coffee {
    CoffeeSize size; // line 1

    public static void main(String... arg)    // line 2
    {
        enum CoffeeSize{ SMALL, NORMAL, BIG ; }    // line 3
        
        Coffee drink = new Coffee();
        drink.size = CoffeeSize.BIG;    // line 4
        
        System.out.println(drink.size);    // line 5
    }
}
