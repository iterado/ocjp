package enums;

protected enum Animal {
    DOG, CAT, FISH
} // line 1

public class TestEnumAnimal {
    Animal[] animals = Animal.values(); // line 2
    static Animal woofy = new Animal(); // line 3

    public static void main(String... arg) {
        System.out.println(woofy);
        for (Animal animal : animals) { // line 4
            System.out.println(animal);
        }
    }
}