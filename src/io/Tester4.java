package io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public class Tester4 {

    public static void main(String[] args) {
        BufferedWriter bw1 = new BufferedWriter(new File("data.txt"));
        BufferedWriter bw2 = new BufferedWriter(new FileWriter("data.txt"));
        BufferedWriter bw3 = new BufferedWriter(new PrintWriter("data.txt"));
        BufferedWriter bw4 = new BufferedWriter("data.txt");
    }
}
