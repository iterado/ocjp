package io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;

public class Test1 {

    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter writer1 = new PrintWriter("file.dat");
        PrintWriter writer2 = new PrintWriter(new BufferedWriter("file.dat"));
        PrintWriter writer3 = new PrintWriter(new FileWriter("file.dat"));
        PrintWriter writer4 = new PrintWriter(new File("file.dat"));
    }
}
